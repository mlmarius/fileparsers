from . import base
from . import util
import re


class MSKExtractor(base.BaseLineParser):

    def __init__(self):
        super(MSKExtractor, self).__init__()
        self._regex = re.compile(r"^^\s+\d+\s+(?P<sta>[A-Z0-9]{3,5})_(?P<chan>[A-Z]{3})\s+(?P<acc>-?[\d\.]+)\s+(?P<int>[IVX]+)\s*$")
        self.reached_content = False

    def do_parse(self, line):
        raw = line.raw
        if self.reached_content is False and raw.strip() == '':
            return self.confirm(line)
        self.reached_content = True
        matches = self._regex.match(raw)
        if matches:
            groups = matches.groupdict()
            sta = groups['sta']
            chan = groups['chan']
            acc = util.float_or_none(groups['acc'])
            intensity = util.roman_to_decimal(groups['int'])
            intensities = self.fileParser.data_bucket.setdefault('intensities', {})
            sta_intensities = intensities.setdefault(sta, {})
            sta_intensities[chan] = {"acc": acc, "int": intensity}
            return self.confirm(line)
        return self.reject(line)

class StaPgvPgaExtractor(base.BaseLineParser):

    def __init__(self):
        super(StaPgvPgaExtractor, self).__init__()
        self._regex = re.compile(r"^\s+(\*\s+\d+\s+)?(?P<sta>[A-Z0-9]{3,5})\s+(?P<chan>[A-Z]{3})(?P<other>.*)")
        self.reached_content = False

    def do_parse(self, line):
        raw = line.raw
        if self.reached_content is False and raw.strip() == '':
            return self.confirm(line)
        self.reached_content = True
        matches = self._regex.match(raw)
        if matches:
            groups = matches.groupdict()
            sta = groups['sta']
            chan = groups['chan']
            other = groups['other']

            pgv = util.float_or_none(other[:12].strip())
            pga = util.float_or_none(other[12:].strip())

            measurements = self.fileParser.data_bucket.setdefault('pgv_pga', {})
            sta = measurements.setdefault(sta, {})
            sta[chan] = {'pgv': pgv, 'pga': pga}
            return self.confirm(line)
        return self.reject(line)


class ReportParser(base.BaseFileParser):

    def get_line_parser_container(self):
        return base.LineParserContainer(self, [
            base.SkipTo('Sta       Chan PGV(cm/sec) PGA(cm/sec*2)'),
            StaPgvPgaExtractor(),
            base.SkipTo('Stations max. horizontal acceleration and MSK intensity'),
            MSKExtractor(),
            base.ConsumeAll()
        ])