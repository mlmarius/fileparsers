from . import base
from . import util
from dateutil.parser import parse
import re



class StationMomentExtractor(base.BaseLineParser):

    def __init__(self):
        super(StationMomentExtractor, self).__init__()
        self._regex = re.compile(r"^\s*(?P<sta>[A-Z0-9]{3,5})\s+(?P<chamw>[A-Z]{3})\s+(?P<mw>[0-9\.]+)\s+.*")

    def do_parse(self, line):
        raw = line.raw.strip()
        matches = self._regex.match(raw)
        if matches:
            self.fileParser.data_bucket.setdefault('station_magnitudes', []).append(
                {
                    'sta': matches.group('sta'),
                    'mw': util.float_or_none(matches.group('mw'))
                }
            )
            return self.confirm(line)
        return self.reject(line)


class EventParametersFinder(base.BaseLineParser):

    def do_parse(self, line):
        # confirm all lines until we find our hit line
        raw = line.raw.strip()
        if not raw.startswith("Event local time"):
            return self.confirm(line)

        # we now have our hint line

        # skip the blank line
        self.fileParser.get_line()

        # ex: Thu May 02,2019 05:58:17 GMT N45.76 E26.66 Ml: 3.0 Depth:  71km ID: 20
        line2 = self.fileParser.get_line()
        raw = line2.raw.strip()
        parts = raw.split('GMT')
        parts = map(lambda x: x.strip(), parts)
        dt = parse(parts[0])
        parts = parts[1].split(' ')
        lat, lon = map(lambda x: float(x[1:]), parts[:2])
        ml = float(parts[3])


        self.fileParser.data_bucket['event_parameters'] = {
            "dateTime": dt.isoformat(),
            "lat": lat,
            "lon": lon,
            "ml": ml
        }

        return self.reject(self.fileParser.get_line())


class WaveformMeasurementsExtractor(base.BaseLineParser):
    def __init__(self):
        super(WaveformMeasurementsExtractor, self).__init__()
        self._regex = re.compile(
            r"\s*(?P<sta>[A-Z0-9]{3,5})\s+(?P<chan>[A-Z]{3})\s+(?P<dist>[0-9\.]+)\s+(?P<ml>[0-9\.]+)\s+.*")

    def do_parse(self, line):
        matches = self._regex.match(line.raw)
        if matches:
            raw = line.raw
            self.fileParser.data_bucket.setdefault('waveform_measurements', []).append(
                {
                    "sta": raw[:8].strip(),
                    "chan": raw[8:13].strip(),
                    "dist": util.float_or_none(raw[13:19].strip()),
                    "ml": util.float_or_none(raw[19:25].strip()),
                    "pga": util.float_or_none(raw[25:33].strip()),
                    "pgv": util.float_or_none(raw[33:40].strip()),
                    "psa03": util.float_or_none(raw[40:47].strip()),
                    "psa10": util.float_or_none(raw[47:54].strip()),
                    "psa30": util.float_or_none(raw[54:61].strip()),
                    "arias": util.float_or_none(raw[61:69].strip()),
                    "housner": util.float_or_none(raw[69:77].strip())
                }
            )
            return self.confirm(line)
        return self.reject(line)


class ReportParser(base.BaseFileParser):

    def get_line_parser_container(self):
        return base.LineParserContainer(self, [
            EventParametersFinder(),
            base.SkipTo('sta  chamw  mw       m0       f0    eqR   timePmw    Pmw  timeSmw    Smw  seg'),
            StationMomentExtractor(),
            base.SkipTo('sta    chan  d(km)   ml   pga    pgv   psa03  psa10  psa30   Arias  Housner'),
            WaveformMeasurementsExtractor(),
            base.ConsumeAll()
        ])
