import logging
import re

re_type = type(re.compile(r'test'))

logger = logging.getLogger('fileparser')

class ParsedLine:

    def __init__(self, line_number, raw, success=False, results=None):
        self.raw = raw.rstrip()
        self._results = results
        self.line_number = line_number
        self.success = success
        self.message = None
        self.parsed_with = []
        self.retries = 0

    def __str__(self):
        return "{:15d}: {}".format(self.line_number, self.raw)

    def set_success(self, success, message=None):
        self.success = success
        self.message = message

    def add_parsed_with(self, parser, message=None):
        self.parsed_with.append((parser, message))


class BaseLineParser(object):

    def __init__(self):
        self.index = None
        self.container = None
        self.fileParser = None
        self._confirms = 0
        self._is_rejecting = False
        self._lines = []

    def __str__(self):
        return self.__class__.__name__

    def reset(self):
        self._lines = []
        self._confirms = 0
        self._is_rejecting = False

    def start_rejecting(self):
        self._is_rejecting = True

    def set_index(self, idx):
        self.index = idx

    def set_container(self, container):
        self.container = container

    def set_file_parser(self, file_parser):
        self.fileParser = file_parser

    def add_meta(self, parsed_line, message=None):
        parsed_line.add_parsed_with(self, message)

    def confirm(self, parsed_line, message=None):
        # whenever a parser confirms a line
        # more lines will be fed to it
        parsed_line.set_success(True, message=message)
        self.add_meta(parsed_line, message)
        return parsed_line

    def reject(self, parsed_line, message=None):
        # when a parser rejects a line, the fileParser will requeue this line
        # and then try the next parser
        parsed_line.set_success(False, message=message)
        self.add_meta(parsed_line, message)
        return parsed_line

    def parse(self, line):
        if self._is_rejecting:
            return self.reject(line)

        return self.do_parse(line)

    def do_parse(self, line):
        raise Exception('must implement')
        # parsed_line = raw.next()
        # return self.reject(parsed_line)


class LineParserContainer:
    def __init__(self, file_parser, line_parser_list):
        self._lineParsers = []
        self.fileParser = file_parser
        self._crtParserIdx = None
        self.register_line_parsers(line_parser_list)

    def register_line_parsers(self, line_parsers):
        for parser in line_parsers:
            self.add_line_parser(parser)
        self.select_parser(0)

    def get_file_parser(self):
        return self.fileParser

    def add_line_parser(self, parser):
        crt_idx = len(self._lineParsers)
        parser.set_index(crt_idx)
        parser.set_container(self)
        parser.set_file_parser(self.fileParser)
        self._lineParsers.append(parser)

    def all(self):
        return self._lineParsers

    def select_parser(self, idx):
        if 0 <= idx <= len(self._lineParsers):
            self._crtParserIdx = idx
        else:
            raise Exception('Can not select parser {}'.format(idx))
        return self.get()

    def get(self):
        return self._lineParsers[self._crtParserIdx]

    def select_next_parser(self):
        return self.select_parser(self._crtParserIdx + 1)

    def select_prev_parser(self):
        return self.select_parser(self._crtParserIdx + 1)


class ParseResult:
    def __init__(self, data, filename, from_line, to_line):
        self.data = data
        self.filename = filename
        self.from_line = from_line
        self.to_line = to_line

    def __str__(self):
        return "{:10} =>{:10} {}".format(self.from_line, self.to_line, self.filename)

class BaseFileParser:
    def __init__(self, file_pointer):
        self.retryLines = []
        if(isinstance(file_pointer, file)):
            self.lines = [ParsedLine(lineNumber, row) for lineNumber, row in enumerate(file_pointer.readlines())]
            self.file = file_pointer.name
        else:
            self.file = file_pointer
            with open(self.file) as f:
                self.lines = [ParsedLine(lineNumber, row) for lineNumber, row in enumerate(f.readlines())]
        self.lineParserContainer = self.get_line_parser_container()
        self.data_bucket = {}
        self.from_line = None
        self.to_line = None

        # will raise StopIteration when this is True
        self.lines_exhausted = False

    def __str__(self):
        return "{}: {}".format(self.__class__.__name__, self.file)

    def __iter__(self):
        return self

    def next(self):
        return self.parse()

    def get_line(self):
        line = self.lines.pop(0)
        self.to_line = line.line_number
        return line

    def get_line_parser_container(self):
        return LineParserContainer(self, [])

    def get_line_parser(self):
        return self.lineParserContainer.get()

    def retry(self, line):
        line.retries += 1
        self.lines.insert(0, line)

    def reset(self):
        self.data_bucket = {}
        self.from_line = None
        self.to_line = None
        for lineParser in self.lineParserContainer.all():
            lineParser.reset()
        self.lineParserContainer.select_parser(0)

    def push_results(self):
        if self._on_ready is not None:
            self._on_ready(self.data_bucket, self.file, self.from_line, self.to_line)

    def parse(self):
        if self.lines_exhausted:
            raise StopIteration
        while True:
            try:
                line = self.get_line()
                if self.from_line is None:
                    self.from_line = line.line_number
            except IndexError:
                # can't get any more lines from this file
                result = ParseResult(self.data_bucket, self.file, self.from_line, self.to_line)
                self.lines_exhausted = True
                return result


            if line.retries > 1:
                raise Exception("Too many retries ({}) for line {} ".format(line.retries, line))

            try:
                logger.info("{:>30}({}) -> {}".format(self.get_line_parser(), self.get_line_parser().index, line))
                result = self.get_line_parser().parse(line)
            except Exception as e:
                raise e

            if result.success is True:
                continue

            # this line was rejected by the current parser

            try:
                # try to parse it with the next parser
                self.lineParserContainer.select_next_parser()
            except IndexError:
                # there are no more parsers
                # we try all the parsers again and clear the collected data
                # self.push_results()
                result = ParseResult(self.data_bucket, self.file, self.from_line, self.to_line)
                self.reset()
                return result
            finally:
                self.retry(result)


##############################
#   Helpful line parsers
###

# Skip to a particular line matched by regex or string equality
class SkipTo(BaseLineParser):
    def __init__(self, searched):
        super(SkipTo, self).__init__()

        if isinstance(searched, re_type):
            self._searched = searched
            self.do_parse = self.parse_by_regex
        else:
            self._searched = searched.strip()
            self.do_parse = self.match_string

    def parse_by_regex(self, line):
        if self._searched.match(line.raw):
            return self.reject(self.fileParser.get_line())
        return self.confirm(line)

    def match_string(self, line):
        if line.raw.strip() == self._searched:
            return self.reject(self.fileParser.get_line())
        return self.confirm(line)

# Consume all lines until the end of the file
class ConsumeAll(BaseLineParser):
    def do_parse(self, line):
        return self.confirm(line)