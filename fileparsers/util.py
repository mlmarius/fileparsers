def on_exception_none(func):
    def inner(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except Exception:
            result = None
        return result
    return inner

def roman_to_decimal(val):
    return {
        "I": 1, "II": 2, "III": 3, "IV": 4, "V": 5, "VI": 6, "VII": 7, "VIII": 8, "IX": 9, "X": 10, "XI": 11, "XII": 12
    }[val]


float_or_none = on_exception_none(float)
int_or_none = on_exception_none(int)