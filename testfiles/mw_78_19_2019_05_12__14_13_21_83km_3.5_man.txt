
		Moment magnitude and waveform measurements

		Romania - Event 19 Zona seismica Vrancea
		Event local time: Sun May 12,2019 17:13:21

    Sun May 12,2019 14:13:21 GMT N45.77 E26.64 Ml: 3.5 Depth:  83km ID: 78

     Measurements are performed on data extracted and processed by an operator

	ML: 3.49 Mw: 2.95 netm0: 0.827E+14 netf0: 2.89 neteqR: 0.65 Nsta: 8


		Station moment magnitude estimates - dbmw 

  sta  chamw  mw       m0       f0    eqR   timePmw    Pmw  timeSmw    Smw  seg
  VLDR  HHT   3.5  0.251E+15   4.26   0.39  14:13:41    db  14:13:56     db  V
  COVR  HHT   2.4  0.587E+13   4.45   0.38  14:13:34    db  14:13:43     db  V
  PLOR  HHT   2.4  0.579E+13   2.66   0.63  14:13:33    db  14:13:42     db  V
 BISRR  HHT   3.3  0.114E+15   2.25   0.74  14:13:34    db  14:13:45     db  V
  BOSR  HHT   3.4  0.183E+15   2.51   0.67  14:13:37    db  14:13:50     db  V
   MLR  HHT   3.0  0.384E+14   1.71   0.98  14:13:36    db  14:13:47     db  V
   VRI  HHT   2.6  0.112E+14   3.44   0.49  14:13:33    db  14:13:42     db  V
  ONER  HHT   3.0  0.530E+14   1.83   0.91  14:13:36    db  14:13:47     db  V

		Waveform measurements

 sta    chan  d(km)   ml   pga    pgv   psa03  psa10  psa30   Arias  Housner
  VLDR  HHN   112   3.8    0.20   0.00   0.10   0.01         0.0000  0.0094
  VLDR  HHE   112   3.8    0.22   0.00   0.05   0.01         0.0000  0.0087
  COVR  HHZ    34   2.9    0.03   0.00   0.01   0.00         0.0000  0.0012
  COVR  HHN    34   2.9    0.04   0.00   0.01   0.00         0.0000  0.0011
  COVR  HHE    34   2.9    0.03   0.00   0.01   0.00         0.0000  0.0011
  PLOR  HHZ     9   2.8    0.02   0.00   0.02   0.00                 0.0018
  PLOR  HHN     9   2.8    0.04   0.00   0.06   0.00         0.0000  0.0036
  PLOR  HHE     9   2.8    0.02   0.00   0.04   0.00                 0.0030
  SULR  HHN   124   3.7    0.02   0.00   0.04                        0.0026
  DOPR  HHZ    99   3.4    0.02   0.00   0.00   0.00                 0.0006
  DOPR  HHE    99   3.4    0.05   0.00   0.01   0.01   0.00  0.0000  0.0020
 BISRR  HHZ    24   3.3    0.06   0.00   0.11   0.00         0.0000  0.0043
 BISRR  HHN    24   3.3    0.06   0.00   0.10   0.02         0.0000  0.0077
 BISRR  HHE    24   3.3    0.09   0.00   0.15   0.01         0.0000  0.0114
 NEGRR  HHN    99   3.5    0.07   0.00   0.03   0.00         0.0000  0.0030
  BOSR  HHZ    83   3.8    0.10   0.00   0.03   0.00         0.0000  0.0029
  BOSR  HHN    83   3.8    0.26   0.00   0.11   0.01         0.0000  0.0073
  BOSR  HHE    83   3.8    0.17   0.00   0.08   0.01         0.0000  0.0065
   MLR  HHZ    62   3.3    0.01   0.00   0.01   0.00                 0.0007
   MLR  HHN    62   3.3    0.00   0.00   0.01   0.00                 0.0007
   MLR  HHE    62   3.3    0.00   0.00   0.01   0.00                 0.0008
  PANC  HHZ    41          0.11   0.00   0.02   0.00         0.0000  0.0033
  TESR  HHZ    82   3.3    0.02   0.00   0.01   0.00                 0.0007
  TESR  HHN    82   3.3    0.02   0.00   0.01   0.00                 0.0011
   VRI  HHZ    13   2.9    0.06   0.00   0.02   0.00         0.0000  0.0020
   VRI  HHN    13   2.9    0.02   0.00   0.06   0.00                 0.0028
   VRI  HHE    13   2.9    0.05   0.00   0.13   0.00         0.0000  0.0052
   ISR  HHZ    72   3.5    0.04   0.00   0.03   0.00         0.0000  0.0017
  NEHR  HHZ    46   3.0    0.00   0.00   0.00   0.00                 0.0002
  NEHR  HHE    46   3.0    0.01   0.00   0.00   0.00                 0.0003
  SCTR  HHZ   106   3.6    0.05   0.00   0.02                0.0000  0.0020
  SCTR  HHN   106   3.6    0.06   0.00   0.03   0.00         0.0000  0.0030
  ONER  HHZ    65   3.3    0.02   0.00   0.01   0.00         0.0000  0.0007
  ONER  HHN    65   3.3    0.02   0.00   0.01   0.00         0.0000  0.0010
  ONER  HHE    65   3.3    0.02   0.00   0.01   0.00         0.0000  0.0009
  TURR  HHZ    57   3.7    0.04   0.00   0.02   0.00                 0.0012
  GHRR  HHN    68   3.7    0.11   0.00   0.08   0.01   0.00  0.0000  0.0054

  Units: pgv: cm/sec, pga,psa03,psa10,psa30: cm/sec^2 Arias: cm/sec Housner: cm
