from fileparsers import wfmeas
import glob
import os
import logging

logging.basicConfig(level=logging.INFO)


def on_ready(data_bucket, file_path, from_line, to_line):
    print(data_bucket)
    print("{}({} -> {})".format(file_path, from_line, to_line))


if __name__ == "__main__":
    logging.info('Running tests')
    path = os.path.abspath(os.path.join('../', 'testfiles', 'WFM_*.txt'))
    logging.info(path)
    for filename in glob.glob(path):
        logging.info(filename)
        with open(filename) as f:
            parser = wfmeas.ReportParser(f)
            for data in parser:
                print(data.data)