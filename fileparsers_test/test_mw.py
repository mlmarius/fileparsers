from fileparsers import mw
import glob
import os
import logging

logging.basicConfig(level=logging.WARNING)


def on_ready(data_bucket, file_path, from_line, to_line):
    print(data_bucket)
    print("{}({} -> {})".format(file_path, from_line, to_line))


if __name__ == "__main__":
    logging.info('Running tests')
    path = os.path.abspath(os.path.join('../', 'testfiles', 'mw_*.txt'))
    logging.info(path)
    for filename in glob.glob(path):
        logging.info(filename)
        with open(filename) as f:
            parser = mw.ReportParser(f)
            for data in parser:
                print(data)